import pygame


class Frame:
    def __init__(self, window, window_size):
        self.window = window
        self.window_size = window_size

    def draw(self):
        ws = self.window_size
        pos_list = [(0, 0), (0, ws), (0, 0), (ws, 0), (ws - 1, 0), (ws - 1, ws - 1), (0, ws - 1), (ws - 1, ws - 1)]
        pygame.draw.lines(self.window, (255, 255, 255), False, pos_list, 5)
