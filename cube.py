import pygame


class Cube:
    def __init__(self, start_pos: tuple, color, direction: tuple = (0, 0)):
        self.pos = start_pos
        self.direction_x = direction[0]
        self.direction_y = direction[1]
        self.color = color
        self.size = 20

    def move(self, rel_new_pos: tuple):
        self.direction_x, self.direction_y = rel_new_pos[0], rel_new_pos[1]
        self.pos = (self.pos[0] + self.direction_x, self.pos[1] + self.direction_y)

    def draw(self, window):
        dis = self.size
        row, col = self.pos[0], self.pos[1]
        pygame.draw.rect(window, self.color, (row * dis, col * dis, dis, dis))
