import os
import random

import pygame


class Sounds:
    bg_music = os.path.join('music', 'bg_music.wav')
    bite_sounds = [os.path.join('sounds', 'bite1.wav'),
                   os.path.join('sounds', 'bite2.wav'),
                   os.path.join('sounds', 'bite3.wav')]
    pygame.mixer.init()

    def play_bite_sound(self):
        pygame.mixer.Sound(random.choice(self.bite_sounds)).play()

    def play_bg_music(self, volume: float):
        pygame.mixer.music.load(self.bg_music)
        pygame.mixer.music.set_volume(volume)
        pygame.mixer.music.play(-1)
