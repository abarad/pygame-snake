import random

import pygame

from cube import Cube


class Snake:
    speed = 20
    turns = {}
    tail = []  # snake with no head

    def __init__(self, start_pos: tuple, start_length: int = 3):
        self.color = (0, 255, 0)
        self.direction = self.get_random_direction()
        self.head = Cube(start_pos, color=(255, 0, 0), direction=self.direction)
        self.body = [self.head]  # snake tail + snake head
        self.start_length = start_length
        self.init_length()
        self.direction_x, self.direction_y = self.get_random_direction()

    def init_length(self):
        for i in range(self.start_length):
            self.grow()

    def speed_up(self):
        self.speed += 2

    def is_hit_cube(self, cube: Cube):
        return self.head.pos == cube.pos

    def is_hit_self(self):
        self.tail = self.body[1:]
        return self.head.pos in [cube.pos for cube in self.tail]

    def _set_head_turn(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT] and self.direction_x != 1:
            self.direction_x, self.direction_y = -1, 0
            self.turns[self.head.pos[:]] = [self.direction_x, self.direction_y]
        elif keys[pygame.K_RIGHT] and self.direction_x != -1:
            self.direction_x, self.direction_y = 1, 0
            self.turns[self.head.pos[:]] = [self.direction_x, self.direction_y]
        elif keys[pygame.K_UP] and self.direction_y != 1:
            self.direction_y, self.direction_x = -1, 0
            self.turns[self.head.pos[:]] = [self.direction_x, self.direction_y]
        elif keys[pygame.K_DOWN] and self.direction_y != -1:
            self.direction_y, self.direction_x = 1, 0
            self.turns[self.head.pos[:]] = [self.direction_x, self.direction_y]

    def is_hit_wall(self, window_size):
        return any(
            [self.head.direction_x == -1 and [c.pos[0] for c in self.body if c.pos[0] <= -1],
             self.head.direction_x == 1 and [c.pos[0] for c in self.body if c.pos[0] >= window_size / c.size],
             self.head.direction_y == 1 and [c.pos[1] for c in self.body if c.pos[1] >= window_size / c.size],
             self.head.direction_y == -1 and [c.pos[1] for c in self.body if c.pos[1] <= -1]])

    def move(self):
        self._set_head_turn()
        for index, cube in enumerate(self.body):
            current_pos = cube.pos[:]
            if current_pos in self.turns:
                cube.move(tuple(self.turns[current_pos]))
                if index == len(self.body) - 1:
                    self.turns.pop(current_pos)
            else:
                cube.move((cube.direction_x, cube.direction_y))

    @staticmethod
    def get_random_pos() -> tuple:
        x, y = random.randrange(1, 20), random.randrange(1, 20)
        return x, y

    @staticmethod
    def get_random_direction() -> tuple:
        while True:
            x, y = random.randrange(-1, 2), random.randrange(-1, 2)
            if (x != 0 and y != 0) or (x == 0 and y == 0):
                continue
            return x, y

    def reset(self):
        self.direction = self.get_random_direction()
        self.body = []
        self.turns = {}
        self.head = Cube(self.get_random_pos(), color=(255, 0, 0), direction=self.direction)
        self.body.append(self.head)
        self.speed = 20
        self.direction_x = 1
        self.direction_y = 0
        for i in range(self.start_length):
            self.grow()

    def grow(self):
        tail_tip = self.body[-1]
        dx, dy = tail_tip.direction_x, tail_tip.direction_y
        if dx == 1 and dy == 0:
            self.body.append(Cube((tail_tip.pos[0] - 1, tail_tip.pos[1]), (255, 0, 0)))
        elif dx == -1 and dy == 0:
            self.body.append(Cube((tail_tip.pos[0] + 1, tail_tip.pos[1]), (255, 0, 0)))
        elif dx == 0 and dy == 1:
            self.body.append(Cube((tail_tip.pos[0], tail_tip.pos[1] - 1), (255, 0, 0)))
        elif dx == 0 and dy == -1:
            self.body.append(Cube((tail_tip.pos[0], tail_tip.pos[1] + 1), (255, 0, 0)))
        self.body[-1].direction_x, self.body[-1].direction_y = dx, dy

    def draw(self, window):
        for index, cube in enumerate(self.body):
            cube.draw(window)
