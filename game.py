import os
import random

import pygame

from cube import Cube
from frame import Frame
from snake import Snake
from sounds import Sounds


class Game:
    cube_size = Cube((0, 0), (0, 0, 0)).size
    sounds = Sounds()
    pygame.display.set_icon(pygame.image.load(os.path.join('images', 'snake.ico')))
    pygame.display.set_caption('Pygame Snake')

    def __init__(self, window_size: int):
        self.window_size = window_size
        self.width, self.height = window_size, window_size
        self.window = pygame.display.set_mode((self.width, self.height))
        self.snake = Snake(start_pos=self.get_random_pos())
        self.apple = Cube(self.get_apple_pos(), (0, 255, 80))
        self.frame = Frame(self.window, window_size)

    def redraw_window(self):
        self.window.fill((0, 0, 0))
        self.snake.draw(self.window)
        self.apple.draw(self.window)
        self.frame.draw()
        pygame.display.update()

    def get_random_pos(self) -> tuple:
        rel_size = self.window_size / self.cube_size - 1
        x, y = random.randrange(1, rel_size), random.randrange(1, rel_size)
        return x, y

    def get_apple_pos(self) -> tuple:
        while True:
            x, y = self.get_random_pos()
            snake_body_pos = [cube.pos for cube in self.snake.body]
            if (x, y) in snake_body_pos:
                continue
            else:
                return x, y

    @staticmethod
    def should_quit():
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True
        return False

    def start_over(self):
        self.snake.reset()
        self.apple = Cube(self.get_apple_pos(), color=(0, 255, 0))

    def play(self):
        clock = pygame.time.Clock()
        self.sounds.play_bg_music(volume=0.05)
        while True:
            pygame.time.delay(1)
            clock.tick(self.snake.speed)
            self.snake.move()
            if self.should_quit():
                pygame.quit()
            elif self.snake.is_hit_self():
                self.start_over()
            elif self.snake.is_hit_wall(window_size=self.window_size):
                self.start_over()
            elif self.snake.is_hit_cube(self.apple):
                self.sounds.play_bite_sound()
                self.snake.speed_up()
                self.snake.grow()
                self.apple = Cube(self.get_apple_pos(), color=(0, 255, 0))
            self.redraw_window()


if __name__ == '__main__':
    game = Game(700)
    game.play()
